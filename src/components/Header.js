import React from 'react'
import logo from '../Logo2.png'
import { useAuth0 } from "../auth/react-auth0-wrapper";


const Header = () => {
  const { isAuthenticated, loginWithRedirect, logout } = useAuth0();

  return (
        <header>
            <h1><a href="/"><img alt="Izarkom logoa" src={logo} /><span className="sr-only">Izarkom</span></a></h1>
            <nav>
                <ul>
                    <li><button id="menu"><span className="sr-only">Menu</span></button></li>
                    <li>
                      {isAuthenticated && <button id="login" onClick={() => logout()}>Log out</button>}
                    </li>
                </ul>
            </nav>
        </header>
  );
};

export default Header;
