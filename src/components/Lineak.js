import React, { Component } from 'react'
import { Spinner } from './Spinner.js';
import { gql } from "apollo-boost";
import { Query } from 'react-apollo'
import { Link } from 'react-router-dom'
import { CircularProgressbar } from 'react-circular-progressbar';
import { useAuth0 } from "../auth/react-auth0-wrapper";

const LINEAK = gql`
query LineakByUser($id: String!, $hilabetea: Int!, $urtea: Int!) {
    linea(where: {user: {_eq: $id}}, order_by: {id: asc}) {
        zenbakia
        id
        tarifaFK {
            name
            MB
            minutu
        }
        kontsumoa(where: {hilabetea: {_eq: $hilabetea}, urtea: {_eq: $urtea}}) {
            MB
            minutu
        }
    }
    user_by_pk(auth0_id: $id) {
        name
    }
}`

const Lineak = () => {

    const { user } = useAuth0();

    console.log(user.sub);

    function getNameAtzizki(name){
        return /[AaEeIiOoUuRr]$/.test(name) ? name+"ren" : name+"en";
    }
    return(
        <Query query={LINEAK} variables={{id: user.sub, hilabetea:2, urtea:2020}}  >
        {({ loading, error, data }) => {
            if (loading) return <Spinner color="#bf3e2d" />
            if (error) return <div>Error ${error}  </div>
            
                console.log(data);
            return (
                <div className="lineak">
                <h2>{getNameAtzizki(data.user_by_pk.name)} lineak</h2>
                <ul>
                    {data.linea.map(linea => {
                        const { id, zenbakia, kontsumoa, tarifaFK} = linea;
                        let mb_portzentaia, minutu_portzentaia;
                        if(kontsumoa.length>0){
                        if(kontsumoa[0].MB > tarifaFK.MB && tarifaFK.MB==0){
                            mb_portzentaia=100;
                        } 
                        else{
                            mb_portzentaia = parseFloat((kontsumoa[0].MB / tarifaFK.MB).toFixed(3))* 100;
                        } 
                        if(kontsumoa[0].minutu > tarifaFK.minutu && tarifaFK.minutu==0){
                            minutu_portzentaia=100;
                        } 
                        else{
                            minutu_portzentaia = parseFloat((kontsumoa[0].minutu / tarifaFK.minutu).toFixed(3))* 100;
                        } 
                        } 

                        return  (
                            <li key={id}>
                                <h3>{zenbakia} - {tarifaFK.name}</h3>
                                { kontsumoa.length> 0 ? (
                                    <>
                                <p>Kontsumituriko datuak: {kontsumoa[0].MB} / {tarifaFK.MB}</p>
                                <div className="svg-container">
                                    <CircularProgressbar value={mb_portzentaia} text={`${mb_portzentaia}%`} />
                                </div>
                                <p>Kontsumituriko minutuak: {linea.kontsumoa[0].minutu} / {linea.tarifaFK.minutu}</p>
                                <div className="svg-container">
                                    <CircularProgressbar value={minutu_portzentaia} text={`${minutu_portzentaia}%`} />
                                </div>
                                    </>
                                ) : null} 
                                <p><Link
                                        to={{
                                            pathname: `/tarifa/${id}`,
                                        }}
                                    >Tarifa</Link></p>
                                <p><Link
                                        to={{
                                            pathname: `/kontsumoak/${id}`,
                                        }}
                                    >Kontsumoak</Link></p>
                            </li>
                        )}
                    )}
                </ul>
                <p><Link
                        to={{
                            pathname: "/linea",
                        }}
                    >Linea gehitu</Link></p>
            </div>
            )
        }}
    </Query>
    )
};

export default Lineak;
