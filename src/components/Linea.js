import React, { useState } from "react";
import { Spinner } from './Spinner.js';
import { gql } from "apollo-boost";
import { Query } from 'react-apollo'
import { useMutation } from '@apollo/react-hooks';
import { useAuth0 } from "../auth/react-auth0-wrapper";

const TARIFAK = gql`
query QueryTarifak{
  tarifa {
    id
    name
  }
}`

const TARIFA_MUTATION =gql`
mutation AddTarifa($user_id: String!, $tarifa: Int!, $zenbakia: String!) {
  __typename
  insert_linea(objects: {tarifa: $tarifa, hurrengo_tarifa: $tarifa, user: $user_id, zenbakia: $zenbakia}) {
    affected_rows
  }
}`

const Linea = () => {

    const { user } = useAuth0();
    const [tarifa, setTarifa] = useState(0);
    const [zenbakia, setZenbakia] = useState("");
    const [addTarifa] = useMutation(TARIFA_MUTATION);

    return(
        <Query query={TARIFAK} >
            {({ loading, error, data }) => {
                if (loading) return <Spinner color="#bf3e2d" />
                if (error) return <div>Error ${error}  </div>
                    return(
                        <div className="tarifak">
                            <h2>Linea gehitu</h2>
                                    <form onSubmit={e => {
                                        e.preventDefault();
                                        addTarifa({ variables: { user_id: user.sub, zenbakia, tarifa} });

                                    }}>
                                    <label>
                                        Zenbakia:
                                        <input type="text" value={zenbakia} onChange= {e => setZenbakia(e.target.value)} />
                                    </label>
                                    <label>
                                        Tarifa:
                                        <select value={tarifa} onChange={e => setTarifa(e.target.value)}>
                                            {data.tarifa.map(tarifa => (
                                                <option value={tarifa.id}>{tarifa.name}</option>
                                            ))}
                                        </select>
                                    </label>
                                    <input type="submit" value="Submit" />
                                </form> 
                        </div>
                    )
            }}
        </Query>
    )
}

export default Linea;
