import React, { useState } from 'react'
import { Spinner } from './Spinner.js';
import { gql } from "apollo-boost";
import { Query } from 'react-apollo'
import { useMutation } from '@apollo/react-hooks';

const TARIFAK = gql`
query QueryTarifak($id: Int!) {
  linea_by_pk(id: $id) {
    tarifaFK {
      id
      name
    }
    hurrengoTarifaFK {
      id
      name
    }
  }
  tarifa {
    id
    name
  }
}`

const TARIFA_MUTATION =gql`
mutation MyMutation($id: Int!, $tarifa: Int!) {
  __typename
  update_linea(where: {id: {_eq: $id}}, _set: {hurrengo_tarifa: $tarifa}) {
    returning {
      hurrengoTarifaFK {
        name
      }
    }
  }
}`

const Tarifa = (props) => {
    const [tarifa, setTarifa] = useState(0);
    const [hurrengoTarifa, setHurrengoTarifa] = useState(0);
    const [id] = useState(props.match.params.id);
    const [mutateTarifa] = useMutation(
        TARIFA_MUTATION,
        {
            update(cache, { data: { mutateTarifa } }) {
                const { linea_by_pk } = cache.readQuery({ query: TARIFAK, variables: {id:id}});
                console.log(linea_by_pk);
                console.log(mutateTarifa);
                cache.writeQuery({
                    query: TARIFAK,
                    data: { linea_by_pk: {...linea_by_pk, mutateTarifa }},
                });
            }
        }
    );

    return(
        <Query query={TARIFAK} variables={{id:id}}  >
            {({ loading, error, data }) => {
                if (loading) return <Spinner color="#bf3e2d" />
                if (error) return <div>Error ${error}  </div>

                console.log(hurrengoTarifa)
                setHurrengoTarifa(data.linea_by_pk.hurrengoTarifaFK.name);
                console.log(hurrengoTarifa)
                return (
                    <div className="tarifak">
                        <h2>Zure tarifa</h2>
                        <h3>Oraingo tarifa:{data.linea_by_pk.tarifaFK.name}</h3>
                        <h3>Hurrengo hilabetetik aurrera izango duzun tarifa:{ hurrengoTarifa }</h3>
                        <div onChange={e => setTarifa(e.target.value)}>
                            {data.tarifa.map(tarifa => (
                                <>
                                {(tarifa.id != data.linea_by_pk.tarifaFK.id) ?
                                    <label htmlFor={tarifa.id}>
                                    <input type="radio" id={tarifa.id} value={tarifa.id} name="tarifa"></input>
                                    {tarifa.name}
                                    </label> : null }
                                </>
                            ))}
                        </div>
                        <button onClick={() => {mutateTarifa({ variables: { id, tarifa} })}}>Aldatu</button>
                    </div>
                )
            }}
        </Query>
    )
}

export default Tarifa;



