import React, { useState } from "react";
import '../styles/index.scss';
import Header from './Header.js';
import Lineak from './Lineak.js';
import Kontsumoak from './Kontsumoak.js';
import Tarifa from './Tarifa.js';
import Linea from './Linea.js';
import { Switch, Route } from 'react-router-dom'
import {ApolloProvider} from "react-apollo"
import { ApolloClient, HttpLink, InMemoryCache } from "apollo-boost";
import { setContext } from "apollo-link-context";
import { useAuth0 } from "../auth/react-auth0-wrapper";



const App = () => {

  const { isAuthenticated, loginWithRedirect, user } = useAuth0();

  // used state to get accessToken through getTokenSilently(), the component re-renders when state changes, thus we have
  // our accessToken in apollo client instance.
  const [accessToken, setAccessToken] = useState("");

  const { getTokenSilently, loading } = useAuth0();
  if (loading) {
    return "Loading...";
  }

  // get access token
  const getAccessToken = async () => {
    // getTokenSilently() returns a promise
    try {
      const token = await getTokenSilently();
      setAccessToken(token);
    } catch (e) {
      console.log(e);
    }
  };
  getAccessToken();

  // for apollo client
  const httpLink = new HttpLink({
    uri: "https://izarkom-graphql.herokuapp.com/v1/graphql",
  });

  const authLink = setContext((_, { headers }) => {
    const token = accessToken;
    if (token) {
      return {
        headers: {
          ...headers,
          authorization: `Bearer ${token}`
        }
      };
    } else {
      return {
        headers: {
          ...headers
        }
      };
    }
  });

  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
  });

    return (
    <ApolloProvider client={client}>
            <div className="App">
                <Header />
                <main>
                {!isAuthenticated && (
                    <>
                    <p>Saioa hasi behar duzu</p>
                    <button id="login" onClick={() => loginWithRedirect({})}>Log in</button>
                    </>
                )}
                {isAuthenticated && (
                <Switch>
                  <Route exact path="/" component={Lineak} />
                  <Route exact path="/kontsumoak/:id" component={Kontsumoak} />
                  <Route exact path="/tarifa/:id" component={Tarifa} />
                  <Route exact path="/linea" component={Linea} />
                </Switch>
                )}
                </main>
            </div>
    </ApolloProvider>
    );
}

export default App;
