import React, { useState } from "react";
import { Spinner } from './Spinner.js';
import { gql } from "apollo-boost";
import { Query } from 'react-apollo'
import { Bar } from 'react-chartjs-2';
import 'chartjs-plugin-annotation';

const LINEAK = gql`
query KontsumoakByUser($id: Int!, $urtea: Int!) {
  linea_by_pk(id: $id) {
    kontsumoa(where: {urtea: {_eq: $urtea}}) {
      MB
      minutu
      hilabetea
    }
    kontsumoa_aggregate(where: {urtea: {_lt: $urtea}}) {
      aggregate {
        count
      }
    }
    zenbakia
    tarifaFK{
      MB
      minutu
      mugagabea
    }
  }
}`

const Kontsumoak = (props) => {
    const [urtea, setUrtea] = useState(new Date().getFullYear());
    const [id] = useState(props.match.params.id);
    console.log(id);

    const getFormattedData = (data) => {
        let mb_array= Array(12).fill(0);
        for(let kontsumoa of data){
            mb_array[kontsumoa.hilabetea-1]=kontsumoa.MB;
        }

        return {
            labels: ["Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"],
            datasets: [
                {
                    data: mb_array,
                    backgroundColor: "#bf3e2d",
                    label: "MB kontsumoa"
                }
            ]
        }
    }
    const getFormattedMinutu = (data) => {
        let minutu_array= Array(12).fill(0);
        for(let kontsumoa of data){
            minutu_array[kontsumoa.hilabetea-1]=kontsumoa.minutu;
        }

        return {
            labels: ["Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"],
            datasets: [
                {
                    data: minutu_array,
                    backgroundColor: "#bf3e2d",
                    label: "Minutu kontsumoa"
                }
            ]
        }
    }
    const getOptionsMB = (options, data) => {
        let options_mb=JSON.parse(JSON.stringify(options))
        options_mb.scales.yAxes[0].ticks.stepSize=data.linea_by_pk.tarifaFK.MB/5;
        options_mb.annotation.annotations[0].value=data.linea_by_pk.tarifaFK.MB;
        options_mb.annotation.annotations[0].label.yAdjust=data.linea_by_pk.tarifaFK.MB ? 7 : -7;
        return options_mb;
    }
    const getOptionsMinutu = (options, data) => {
        let options_minutu=JSON.parse(JSON.stringify(options))
        options_minutu.scales.yAxes[0].ticks.stepSize=data.linea_by_pk.tarifaFK.minutu ? data.linea_by_pk.tarifaFK.minutu/5 : 5;
        if(!data.linea_by_pk.tarifaFK.mugagabea){
            options_minutu.annotation.annotations[0].value=data.linea_by_pk.tarifaFK.minutu;
            options_minutu.annotation.annotations[0].label.yAdjust=data.linea_by_pk.tarifaFK.minutu ? 7 : -7;
        }
        return options_minutu;
    }
    return(
        <Query query={LINEAK} variables={{id: id, urtea: urtea}}>
            {({ loading, error, data }) => {
                if (loading) return <Spinner color="#bf3e2d" />
                    if (error) return <div>Error ${error}  </div>

                    const options={
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                }
                            }]
                        },
                        annotation: {
                            annotations: [{
                                type: 'line',
                                mode: 'horizontal',
                                scaleID: 'y-axis-0',
                                borderColor: 'rgb(75, 192, 192)',
                                borderWidth: 4,
                                label: {
                                    enabled: true,
                                    yAdjust: -7,
                                    fontColor: "#000",
                                    backgroundColor: 'transparent',
                                    content: 'Kontratatutakoa'
                                }
                            }]
                        }
                    }
                const options_mb=getOptionsMB(options, data);
                const options_minutu=getOptionsMinutu(options, data);
                console.log(data.linea_by_pk.kontsumoa);
                console.log(getFormattedData(data.linea_by_pk.kontsumoa))
                console.log(options);
                return (
                    <div className="kontsumoak">
                        <h2>{data.linea_by_pk.zenbakia} lineako kontsumoak</h2>
                        { data.linea_by_pk.kontsumoa_aggregate.aggregate.count > 0 ?
                                <div className="aurrekoa"  value={urtea-1} onClick={e => setUrtea(parseInt(e.target.innerHTML))}>{urtea-1}</div> : null }
                                { urtea < 2020 ?
                                        <div className="hurrengoa" value={urtea+1} onClick={e => setUrtea(parseInt(e.target.innerHTML))}>{urtea+1}</div> : null }
                                        <h3>{urtea}</h3>
                                        <Bar options={options_mb} data={getFormattedData(data.linea_by_pk.kontsumoa)} />
                                        <Bar options={options_minutu} data={getFormattedMinutu(data.linea_by_pk.kontsumoa)} />
                                    </div>
                )
            }}
        </Query>
    )
}

export default Kontsumoak;
